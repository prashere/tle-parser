# TleParser

**Elixir Library to parse TLE (Two-Line Element) data.**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `tle_parser` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:tle_parser, "~> 0.1.0"}
  ]
end
```

## Changes

All changes made to the library can be found in [CHANGELOG.md](CHANGELOG.md)
