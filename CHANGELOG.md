# Changelog

All notable changes to this library will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0)

## [Unreleased]
### Added
- `TLE.Parser` module to parse TLE data.
- `TLE.Parser.parse_file` function to parse from file.
- `TLE.Parser.parse` function to parse from IO stream & string.
- `TLE.Object` module to represent Object data.
- `%TLE.Object{}` struct with following fields,
  - `name`
  - `number` to denote catalog number
  - `classification`
  - `intl_desig` to denote international designator
  - `epoch_date_time`
  - `type` to denote if it's a satellite or rocket/booster body or space debris
- `TLE.Object` utility methods
  - `get_launch_year`
  - `get_launch_number`
  - `get_launch_piece`
  - `get_epoch_year`
  - `get_epoch_day`
  - `get_epoch_time`
  - `get_epoch_time_for_nasa_skywatch`
- Unit Test cases covering all the above
