defmodule TLE.Parser do
  @moduledoc """
  Documentation for `TleParser`.
  """

  def parse_file(file_path) do
    file_path
    |> File.stream!()
    |> Stream.chunk_every(3)
    |> Enum.map(&parse(&1))
  end

  def parse(data) when is_binary(data) do
    {:ok, stream} = StringIO.open(data)

    stream
    |> IO.binstream(:line)
    |> parse()
  end

  def parse(stream) do
    stream
    |> remove_new_line()
    |> trim_white_spaces()
    |> Stream.with_index(0)
    |> Enum.reduce(%TLE.Object{}, &parse_line(&2, &1))
  end

  defp parse_line(object, {line, 0}) do
    type =
      cond do
        String.match?(line, ~r/R\/B|(?i)r(?-i)ocket/) -> :rocket
        String.match?(line, ~r/DEB$|DEB( )+/) -> :debris
        true -> :satellite
      end

    %TLE.Object{object | name: line, type: type}
  end

  defp parse_line(object, {line, 1}) do
    number = String.slice(line, 2, 5)
    classification = String.at(line, 7)
    intl_desig = String.trim(String.slice(line, 9, 7))
    epoch = String.slice(line, 18, 14)

    %TLE.Object{
      object
      | classification: classification,
        number: String.to_integer(number),
        intl_desig: intl_desig,
        epoch_date_time: epoch
    }
  end

  defp parse_line(object, {_, 2}) do
    object
  end

  defp remove_new_line(stream) do
    Stream.map(stream, &String.replace(&1, "\n", ""))
  end

  defp trim_white_spaces(stream) do
    Stream.map(stream, &String.trim(&1))
  end
end
