defmodule TLE.Object do
  defstruct name: nil,
            number: nil,
            classification: nil,
            intl_desig: nil,
            epoch_date_time: nil,
            type: nil

  def get_launch_year(%TLE.Object{intl_desig: nil}) do
    {:error, nil}
  end

  def get_launch_year(%TLE.Object{intl_desig: intl_desig}) do
    String.slice(intl_desig, 0, 2)
    |> String.to_integer()
    |> get_full_year()
  end

  def get_launch_number(%TLE.Object{intl_desig: nil}) do
    {:error, nil}
  end

  def get_launch_number(%TLE.Object{intl_desig: intl_desig}) do
    String.slice(intl_desig, 2, 3)
    |> String.to_integer()
  end

  def get_launch_piece(%TLE.Object{intl_desig: nil}) do
    {:error, nil}
  end

  def get_launch_piece(%TLE.Object{intl_desig: intl_desig}) do
    String.slice(intl_desig, 5, 3)
  end

  def get_epoch_year(%TLE.Object{epoch_date_time: nil}) do
    {:error, nil}
  end

  def get_epoch_year(%TLE.Object{epoch_date_time: epoch}) do
    String.slice(epoch, 0, 2)
    |> String.to_integer()
    |> get_full_year()
  end

  def get_epoch_day(%TLE.Object{epoch_date_time: nil}) do
    {:error, nil}
  end

  def get_epoch_day(%TLE.Object{epoch_date_time: epoch}) do
    String.slice(epoch, 2, 3)
    |> String.to_integer()
  end

  def get_epoch_time(%TLE.Object{epoch_date_time: nil}) do
    {:error, nil}
  end

  def get_epoch_time(%TLE.Object{epoch_date_time: _} = object) do
    skywatch_format = get_epoch_time_for_nasa_skywatch(object)
    [_ | epoch_time] = String.split(skywatch_format, ":")

    {:ok, time} =
      Time.new(
        Enum.at(epoch_time, 0) |> String.to_integer(),
        Enum.at(epoch_time, 1) |> String.to_integer(),
        Enum.at(epoch_time, 2) |> String.to_float() |> trunc
      )

    time
  end

  def get_epoch_time_for_nasa_skywatch(%TLE.Object{epoch_date_time: nil}) do
    {:error, nil}
  end

  def get_epoch_time_for_nasa_skywatch(%TLE.Object{epoch_date_time: epoch} = object) do
    day = get_epoch_day(object)
    year = get_epoch_year(object)

    # get rid of year part from epoch string
    epoch = String.slice(epoch, 2, 12)

    hours = get_hours_from_epoch_time(epoch, day)
    minutes = get_minutes_from_hour(hours)
    seconds = get_seconds_from_minutes(minutes)

    "#{year}/#{day}:#{trunc(hours)}:#{trunc(minutes)}:#{Float.floor(seconds, 2)}"
  end

  # private methods starts here

  defp get_full_year(yy) do
    case yy do
      yy when yy >= 57 ->
        "19#{yy}" |> String.to_integer()

      yy when yy <= 56 ->
        "20#{yy}" |> String.to_integer()
    end
  end

  defp get_hours_from_epoch_time(epoch, day) do
    (String.to_float(epoch) - day) * 24
  end

  defp get_minutes_from_hour(hour) do
    (hour - trunc(hour)) * 60
  end

  defp get_seconds_from_minutes(minutes) do
    (minutes - trunc(minutes)) * 60
  end
end
