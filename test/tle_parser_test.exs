defmodule TLEParserTest do
  use ExUnit.Case

  @valid_entry "NOAA 14           
  1 23455U 94089A   97320.90946019  .00000140  00000-0  10191-3 0  2621
  2 23455  99.0090 272.6745 0008546 223.1686 136.8816 14.11711747148495"

  defp invoke_parser_with_valid_entry() do
    {:ok, stream} = StringIO.open(@valid_entry)
    TLE.Parser.parse(IO.binstream(stream, :line))
  end

  test "use binary string to parse" do
    object = TLE.Parser.parse(@valid_entry)
    assert object.name == "NOAA 14"
    assert object.number == 23455
    assert object.classification == "U"
  end

  test "extract name from valid entry" do
    object = invoke_parser_with_valid_entry()
    assert object.name == "NOAA 14"
  end

  test "extract classification from valid entry" do
    object = invoke_parser_with_valid_entry()
    assert object.classification == "U"
  end

  test "extract catalog number from valid_entry" do
    object = invoke_parser_with_valid_entry()
    assert object.number == 23455
  end

  test "extract international designator from valid entry" do
    object = invoke_parser_with_valid_entry()
    assert object.intl_desig == "94089A"
  end

  test "get object 4 digit launch year from valid entry" do
    object = invoke_parser_with_valid_entry()
    assert TLE.Object.get_launch_year(object) == 1994
  end

  test "get object launch number from valid entry" do
    object = invoke_parser_with_valid_entry()
    assert TLE.Object.get_launch_number(object) == 89
  end

  test "get object launch peice value from valid entry" do
    object = invoke_parser_with_valid_entry()
    assert TLE.Object.get_launch_piece(object) == "A"
  end

  test "get type from valid entry" do
    object = invoke_parser_with_valid_entry()
    assert object.type == :satellite
  end

  test "get epoch entry from valid entry" do
    object = invoke_parser_with_valid_entry()
    assert object.epoch_date_time == "97320.90946019"
  end

  test "get epoch full year from valid entry" do
    object = invoke_parser_with_valid_entry()
    assert TLE.Object.get_epoch_year(object) == 1997
  end

  test "get epoch day from valid entry" do
    object = invoke_parser_with_valid_entry()
    assert TLE.Object.get_epoch_day(object) == 320
  end

  test "get epoch time as elixir time from valid entry" do
    object = invoke_parser_with_valid_entry()
    time = TLE.Object.get_epoch_time(object)
    assert Time.compare(time, ~T[21:49:37]) == :eq
  end

  test "get epoch datetime as nasa skywatch formatted string" do
    object = invoke_parser_with_valid_entry()
    skywatch_format = TLE.Object.get_epoch_time_for_nasa_skywatch(object)
    assert skywatch_format == "1997/320:21:49:37.36"
  end
end
